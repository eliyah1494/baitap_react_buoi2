import React, { Component } from "react";
import CardGlass from "./CardGlass";
import stylels from "./style.module.css";
export default class Model extends Component {
  render() {
    let { img, name, price, desc } = this.props;
    return (
      <div>
        <div className={stylels.backgroungGlass}>
          <img className={stylels.imgGlass} src={img} alt="" />
          <div className={`${stylels.descGlass} mt-5`}>
            <h5>{name}</h5>
            <p>{price}</p>
            <p>{desc}</p>
          </div>
        </div>
      </div>
    );
  }
}
