import React, { Component } from "react";
import GlassItem from "./GlassItem";

export default class CardGlass extends Component {
  renderGlassList = (ListGlass) => {
    return ListGlass.map((item, index) => {
      return (
        <GlassItem
          pickGlass={this.props.pickGlass}
          img={item.url}
          name={item.name}
          price={item.price}
          desc={item.desc}
          key={index}
        />
      );
    });
  };

  render() {
    console.log("this.props", this.props);
    let { ListGlass } = this.props;
    return <div className="row">{this.renderGlassList(ListGlass)}</div>;
  }
}
