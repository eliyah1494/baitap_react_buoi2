import React, { Component } from "react";

export default class GlassItem extends Component {
  render() {
    let { img, name, price, desc } = this.props;
    return (
      <div
        className="col-md-2 pt-5"
        onClick={() => {
          this.props.pickGlass(img, name, price, desc);
        }}
      >
        <img src={this.props.img} alt="" style={{ width: 150, height: 50 }} />
      </div>
    );
  }
}
