import React, { Component } from "react";
import CardGlass from "./CardGlass";
import { dataGlass } from "./dataGlass";
import Model from "./Model";
import stylels from "./style.module.css";
export default class Glass extends Component {
  state = {
    glassArr: dataGlass,
    urlImg: "",
    name: "",
    price: "",
    desc: "",
  };

  pickGlass = (img, name, price, desc) => {
    this.setState({
      urlImg: img,
      name: name,
      price: price,
      desc: desc,
    });
  };

  render() {
    let { urlImg, name, price, desc } = this.state;

    return (
      <div className={stylels.bgGlass}>
        <div className={stylels.bgBlack}>
          <div className="container mx-auto">
            <div className="">
              <Model img={urlImg} name={name} price={price} desc={desc} />
            </div>
            <div className={`${stylels.bgwhite} row`}>
              <CardGlass
                pickGlass={this.pickGlass}
                ListGlass={this.state.glassArr}
              />
            </div>
          </div>
        </div>
      </div>
    );
  }
}
